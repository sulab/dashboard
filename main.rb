require 'rubygems'
require 'sinatra'

require 'dm-core'
require 'dm-aggregates'
require 'dm-serializer'

require 'yaml'
require 'slim'
require 'json'
require 'date'

require 'hipchat'
require 'gmail'

require 'awesome_print'
require 'active_support'

class Array
    def histogram ; self.sort.inject({}){|a,x|a[x]=a[x].to_i+1;a} ; end
end

## CONFIGURATION
$yaml = YAML.load_file("settings.yaml")
configure do
  set :port, 3070
  set :public_folder, './public'

  DataMapper.setup(:default, {
    :adapter  => 'mysql',
    :host     => $yaml["mysql"]["host"],
    :username => $yaml["mysql"]["username"],
    :password => $yaml["mysql"]["password"],
    :database => $yaml["mysql"]["database"]
  })

  # Slim::Engine.set_default_options pretty: true
  # DataMapper::Logger.new(STDOUT, :debug)
end
### MODELS

class Log
  include DataMapper::Resource
  property :id,         Serial
  property :application,Integer
  property :version,    String, :length => 12
  property :status,     Integer
  property :header,     String, :length => 255
  property :body,       Text
  property :timestamp,  DateTime
end

class Commit
  include DataMapper::Resource
  property :id,         Serial
  property :author,     String, :length => 255
  property :repo,       String, :length => 255
  property :message,    Text
  property :timestamp,  DateTime
  # property :hash,       String, :length => 40
end

class Mapping
  include DataMapper::Resource
  property :id,          Serial

  property :entrez_id,   String, :length => 20
  #property :title_text,  Text
  property :title_url,   String, :length => 255

  property :updated,     DateTime
end

DataMapper.finalize

### CONTROLLER ACTIONS

before do
end
not_found do
  slim:'404'
end

get '/' do
  slim:'index'
end

# Remote code status report submissions
post '/submit' do
  Log.create(  :application => params['application'],
               :version => params['version'],
               :status => params['status'],
               :header => params['page_title'],
               :body => params['message'],
               :timestamp => DateTime.now )
end

get '/gen_report' do
  content_type :json
  report_generation.to_json
end

get '/weekly_report' do
  report = weekly_report[:pygenewiki]

  rate = (( report[:warning] + report[:error] ) / report[:total].to_f  * 100 ).round(4) rescue 0
  updated_per = ( report[:success] / report[:total].to_f * 100).round(4) rescue 0

  errors = "Top warning issues:\n"
  report[:error_log].sort_by {|k,v| v}.reverse.each do |e|
    errors << "    #{e[0]} (#{ e[1] })\n"
  end

  msg = "A total of #{ report[:total] } pages were checked while #{ report[:success] } (#{updated_per}%) pages were successfully edited.\n\n There was a total of #{ report[:warning] + report[:error] } (#{ rate }%) issues where #{ report[:warning] } give us some notification warning and #{ report[:error] } threw a complete error.\n\n #{errors}"

  color = 'red'
  if rate < 1.0 then color = 'green' end
  hipchat_sender(msg, color)
  email_sender(msg)
end

get '/map' do
  content_type :json
  Mapping.all().to_json(:only => [:entrez_id, :title_url, :updated])
end

get '/map/:id' do
  if (params[:id] =~ /\D/).nil?
    Mapping.first(:entrez_id => params[:id])[:title_url] rescue nil
  else
    Mapping.first(:title_url => params[:id])[:entrez_id] rescue nil
  end
end

post '/map' do
  if !params[:entrez_id].nil? && !params[:title_url].nil?
    Mapping.create( :entrez_id => params[:entrez_id],
                    :title_url => params[:title_url] )
  end
end

get '/biogps-plugins/wp/?:id?' do |id|
  id = id || params[:id]
  if id.nil?
    slim:'biogps-plugins/wp'
  elsif( id =~ /^[0-9]*$/ )
    # confirmed that we were passed a valid formatted id
    target = Mapping.first(:entrez_id => id)[:title_url] rescue nil
    redirect "http://en.wikipedia.org/wiki/#{target}" if !target.nil?
    redirect "http://plugins.biogps.org/cgi-bin/gwgenerator.cgi?id=#{id}"
  else
    #if it didn't match the format of a entrez_id
    redirect "http://en.wikipedia.org/wiki/Special:Search?ns0=1&search=%22Symbol+%3D+#{id}%22&fulltext=Search"
  end
end

get '/biogps-plugins/freebase/?:id?' do |id|
  id = id || params[:id]
  if id.nil?
    slim:'biogps-plugins/freebase'
  end
  #elsif( id =~ /^[0-9]*$/ )
  #i no idea what that perl json stuff is doing
end

get '/biogps-plugins/primerbank/?:id?' do |id|
   @id = id || params[:id]
   slim:'biogps-plugins/primerbank'
end


private

def weekly_report(weeks=1)
  now = DateTime.now
  week_ago = now - ( 7 * weeks )

  errors = []
  Log.all(:application => 0, :status => 1, :timestamp.gte => week_ago ).each do |log|
    errors << log[:body].split('Error: ')[1].strip
  end

  {
    :pygenewiki => {
      :total => Log.all(:application => 0, :timestamp.gte => week_ago ).count,
      :success => Log.all(:application => 0, :status => 0, :body.like => 'Successfully edited%', :timestamp.gte => week_ago ).count,
      :warning => Log.all(:application => 0, :status => 1, :timestamp.gte => week_ago ).count,
      :error => Log.all(:application => 0, :status => 2, :timestamp.gte => week_ago ).count,
      :log => Log.all(:application => 0, :status.not => 0, :order => [:timestamp.desc], :limit => 40, :timestamp.gte => week_ago),
      :error_log => errors.histogram
      }
  }
end

def report_generation
  {
    :pygenewiki => {
      :total => Log.all(:application => 0).count,
      :success => Log.all(:application => 0, :status => 0).count,
      :warning => Log.all(:application => 0, :status => 1).count,
      :error => Log.all(:application => 0, :status => 2).count,
      # :log => Log.all(:application => 0, :status.not => 0, :order => [:timestamp.desc], :limit => 40)    }
      :log => Log.all(:application => 0, :order => [:timestamp.desc], :limit => 4000)    }
  }
end

def get_rand
  o =  [('a'..'z'),('A'..'Z')].map{|i| i.to_a}.flatten;
  string  =  (0..50).map{ o[rand(o.length)]  }.join;
  Digest::MD5.hexdigest(DateTime.now.to_s+string)
end

def hipchat_sender(msg, color=green)
  client = HipChat::Client.new( $yaml["etc"]["hipchat_api"] )
  client[ $yaml["etc"]["hipchat_room"] ].send('Dashy', msg, :color => color)
end

def email_sender(msg)
  gmail = Gmail.connect( $yaml["etc"]["gmail_user"], $yaml["etc"]["gmail_pass"] )
  email = gmail.compose do
    to $yaml["etc"]["email_recipients"]
    subject "Weekly (##{Date.today.cweek}) Su Lab Report"
    body msg
  end
  email.deliver!
end
