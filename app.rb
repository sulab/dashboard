require 'rubygems'
require 'sinatra'

require 'dm-core'
require 'dm-aggregates'
require 'dm-serializer'

require 'yaml'
require 'slim'
require 'json'
require 'date'

require 'hipchat'
require 'gmail'

require 'awesome_print'
require 'active_support'

require './utils/get.rb'

class Array
    def histogram ; self.sort.inject({}){|a,x|a[x]=a[x].to_i+1;a} ; end
end

## CONFIGURATION
$yaml = YAML.load_file("settings.yaml")
configure do
  set :port, 3070
  set :public_folder, './views/public'

  DataMapper.setup(:default, {
    :adapter  => 'mysql',
    :host     => $yaml["mysql"]["host"],
    :username => $yaml["mysql"]["username"],
    :password => $yaml["mysql"]["password"],
    :database => $yaml["mysql"]["database"]
  })

  # Slim::Engine.set_default_options pretty: true
  # DataMapper::Logger.new(STDOUT, :debug)
end

require_relative 'models/init'
require_relative 'routes/init'

private

def weekly_report(weeks=1)
  now = DateTime.now
  week_ago = now - ( 7 * weeks )

  errors = []
  Log.all(:application => 0, :status => 1, :timestamp.gte => week_ago ).each do |log|
    errors << log[:body].split('Error: ')[1].strip
  end

  {
    :pygenewiki => {
      :total => Log.all(:application => 0, :timestamp.gte => week_ago ).count,
      :success => Log.all(:application => 0, :status => 0, :body.like => 'Successfully edited%', :timestamp.gte => week_ago ).count,
      :warning => Log.all(:application => 0, :status => 1, :timestamp.gte => week_ago ).count,
      :error => Log.all(:application => 0, :status => 2, :timestamp.gte => week_ago ).count,
      :log => Log.all(:application => 0, :status.not => 0, :order => [:timestamp.desc], :limit => 40, :timestamp.gte => week_ago),
      :error_log => errors.histogram
      }
  }
end

def report_generation
  {
    :pygenewiki => {
      :total => Log.all(:application => 0).count,
      :success => Log.all(:application => 0, :status => 0).count,
      :warning => Log.all(:application => 0, :status => 1).count,
      :error => Log.all(:application => 0, :status => 2).count,
      # :log => Log.all(:application => 0, :status.not => 0, :order => [:timestamp.desc], :limit => 40)    }
      :log => Log.all(:application => 0, :order => [:timestamp.desc], :limit => 4000)    }
  }
end

def get_rand
  o =  [('a'..'z'),('A'..'Z')].map{|i| i.to_a}.flatten;
  string  =  (0..50).map{ o[rand(o.length)]  }.join;
  Digest::MD5.hexdigest(DateTime.now.to_s+string)
end

def hipchat_sender(msg, color="green")
  client = HipChat::Client.new( $yaml["etc"]["hipchat_api"] )
  client[ $yaml["etc"]["hipchat_room"] ].send('Dashy', msg, :color => color)
end

def email_sender(msg, subject_line, recipients)
  recipients = $yaml["etc"]["email_recipients"] if recipients.nil?

  gmail = Gmail.connect( $yaml["etc"]["gmail_user"], $yaml["etc"]["gmail_pass"] )
  email = gmail.compose do
    to recipients
    subject subject_line
    body msg
  end
  email.deliver!
end
