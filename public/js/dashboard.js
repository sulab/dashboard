var DASH = {
  init : function() {
    $.ajax({
      url: 'gen_report',
      success: function(d) {

        DASH.drawPieGraph([{v:d.pygenewiki.success}, {v:d.pygenewiki.warning}, {v:d.pygenewiki.error}]);
        DASH.drawErrorLog(d.pygenewiki.log);
      }
    });
  },
  drawErrorLog: function(data) {
    var height = 100,
        width = 500,
        color = ["#5bbae1", "#ffe38c", "#ff4444"];

    var tooltip = d3.select("#chart .tooltip")
        .style("position", "absolute")
        .style("z-index", "10")

    var svg = d3.select("#chart").append("svg:svg").attr('height', height),
        rects = svg.selectAll("rect").data(data);

    rects.enter().append("rect")
        .attr('width', 1)
        .attr('height', 100)
        .attr('x', function(d, i){ return i; })
        .attr('y', '0px')
        .attr("fill", function(d, i) {
          return color[d["status"]]
        })
        .on("mouseover", function(obj) {
          return tooltip
                  .style("top", (event.pageY-10)+"px")
                  .style("left",(event.pageX+10)+"px")
                  .style("color", function() {
                    if( obj.status == 0 ) {
                      return "#000";
                    } else {
                      return "red";
                    }
                  })
                  .text( obj.status + " : " +obj.header );
        });

  },
  drawPieGraph : function(data) {
    var w = 400,
        h = 400,
        r = Math.min(w, h)/4,
        labelr = r + 10, // radius for label anchor
        color = ["#5bbae1", "#ffe38c", "#ff4444"],
        donut = d3.layout.pie(),
        arc = d3.svg.arc().innerRadius(0).outerRadius(r);

    var vis = d3.select("#piegraph")
        .append("svg:svg")
        .data([data])
        .attr("width", w + 20)
        .attr("height", h + 20);

    var arcs = vis.selectAll("g.arc")
          .data(donut.value(function(d) { return d.v }))
          .enter().append("svg:g")
          .attr("class", "arc")
          .attr("transform", "translate(" + (r+20) + "," + (r+20) + ")");

    arcs.append("svg:path")
          .attr("fill", function(d, i) { return color[i]; })
          .attr("d", arc);

    arcs.append("svg:text")
      .attr("transform", function(d) {
      var c = arc.centroid(d),
        x = c[0],
        y = c[1],
        // pythagorean theorem for hypotenuse
        h = Math.sqrt(x*x + y*y);
        return "translate(" + (x/h * labelr) +  ',' + (y/h * labelr) +  ")"; 
    })
    .attr("dy", ".35em")
    .attr("text-anchor", function(d) {
      // are we past the center?
      return (d.endAngle + d.startAngle)/2 > Math.PI ? "end" : "start";
    })
    .attr("font-family", "Helvetica")
    .text(function(d){ if(d.value > 0) { return d.value; } });
  }
}

$(document).ready( DASH.init() );
