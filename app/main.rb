require 'rubygems'
require 'sinatra'

require 'dm-core'
require 'dm-aggregates'
require 'dm-serializer'

require 'yaml'
require 'slim'
require 'json'
require 'date'
require 'hipchat'
require 'awesome_print'
require 'active_support'

## CONFIGURATION
$yaml = YAML.load_file("settings.yaml")
configure do
  set :port, 6699
  set :public_folder, '../public'

  DataMapper.setup(:default, {
    :adapter  => 'mysql',
    :host     => $yaml["mysql"]["host"],
    :username => $yaml["mysql"]["username"],
    :password => $yaml["mysql"]["password"],
    :database => $yaml["mysql"]["database"]
  })

  # DataMapper::Logger.new(STDOUT, :debug)
end

### MODELS

class Log
  include DataMapper::Resource
  property :id,         Serial
  property :application,Integer
  property :version,    String, :length => 12
  property :status,     Integer
  property :header,     String, :length => 255
  property :body,       Text
  property :timestamp,  DateTime
end

class Commit
  include DataMapper::Resource
  property :id,         Serial
  property :author,     String, :length => 255
  property :repo,       String, :length => 255
  property :message,    Text
  property :timestamp,  DateTime
  # property :hash,       String, :length => 40
end

DataMapper.finalize

### CONTROLLER ACTIONS

before do
end
not_found do
  slim:'404'
end

get '/' do
  yaml.to_s
  # slim:'index'
end

# Remote code status report submissions
post '/submit' do
  Log.create(  :application => params['application'],
               :version => params['version'],
               :status => params['status'],
               :header => params['page_title'],
               :body => params['message'],
               :timestamp => DateTime.now )
end

get '/gen_report' do
  content_type :json
  report_generation.to_json
end

get '/weekly_report' do
  report = weekly_report[:pygenewiki]

  rate = (( report[:warning] + report[:error] ) / report[:total].to_f  * 100 ).round(4)
  msg = "A total of #{ report[:total] } pages were updated. With a error percentage of #{ rate }% where #{ report[:warning] } give us some notification warning and where #{ report[:error] } threw a complete error."

  if rate < 1.0 then
    hipchat_sender(msg, 'green')
  else
    hipchat_sender(msg, 'red')
  end

end

private

def weekly_report(weeks=1)
  now = DateTime.now
  week_ago = now - (7*weeks)
  {
    :pygenewiki => {
      :total => Log.all(:application => 0, :timestamp.gte => week_ago ).count,
      :success => Log.all(:application => 0, :status => 0, :timestamp.gte => week_ago ).count,
      :warning => Log.all(:application => 0, :status => 1, :timestamp.gte => week_ago ).count,
      :error => Log.all(:application => 0, :status => 2, :timestamp.gte => week_ago ).count,
      :log => Log.all(:application => 0, :status.not => 0, :order => [:timestamp.desc], :limit => 40, :timestamp.gte => week_ago)    }
  }
end

def report_generation
  {
    :pygenewiki => {
      :total => Log.all(:application => 0).count,
      :success => Log.all(:application => 0, :status => 0).count,
      :warning => Log.all(:application => 0, :status => 1).count,
      :error => Log.all(:application => 0, :status => 2).count,
      # :log => Log.all(:application => 0, :status.not => 0, :order => [:timestamp.desc], :limit => 40)    }
      :log => Log.all(:application => 0, :order => [:timestamp.desc], :limit => 4000)    }
  }
end

def get_rand
  o =  [('a'..'z'),('A'..'Z')].map{|i| i.to_a}.flatten;
  string  =  (0..50).map{ o[rand(o.length)]  }.join;
  Digest::MD5.hexdigest(DateTime.now.to_s+string)
end

def hipchat_sender(msg, color=green)
  client = HipChat::Client.new( $yaml["etc"]["hipchat_api"] )
  client[ $yaml["etc"]["hipchat_room"] ].send('Dashy', msg, :color => color)
end

 #  //Cron Jobs
 # public function _writecommits()
 # {
 #    $crud_model = new Model_Crud();
 #    //Get list of all public sulab repos
 #    $request = Request::factory("https://api.bitbucket.org/1.0/users/sulab/")->method('GET')->execute();
 #    $response = json_decode( $request->body() );
 #    //krumo($response->repositories);
 #    if(isset($response)) {
 #    foreach($response->repositories as $repo):
 #      //For each of the repos, get the latest commits
 #      $request = Request::factory("https://api.bitbucket.org/1.0/repositories/sulab/".$repo->slug."/changesets")->method('GET')->execute();
 #      $response = json_decode( $request->body() );
 #      if(isset($response)) {
 #      foreach($response->changesets as $commit):
 #        //krumo($commit);
 #        $crud_model->insertNewest("commits", array(  
 #          "author" => $commit->author,
 #          "repo" => $repo->slug,
 #          "message" => $commit->message,
 #          "timestamp" => $commit->utctimestamp,
 #          "hash" => $commit->raw_node ), "hash" );
 #      endforeach;
 #      }
 #    endforeach;
 #    }
 #  }

 #  public function _writeTwitterMembers()
 #  {
 #    $crud_model = new Model_Crud();
 #    $lab_members = Kohana::$config->load('site')->get('twitter_accounts');
 #    foreach($lab_members as $twitter_account):
 #      $request = Request::factory("http://api.twitter.com/1/statuses/user_timeline.json?screen_name=".$twitter_account)->method('GET')->execute();
 #      //krumo($request->body());
 #      $response = json_decode( $request->body() );
 #      if(isset($response)) {
 #        foreach($response as $status_update):
 #          $post = array();
 #          $post["status_id"] = $status_update->id_str;
 #          $datetime = new DateTime($status_update->timestamp);
 #          $datetime->setTimezone(new DateTimeZone('PST'));
 #          $post["timestamp"] = $datetime->format('Y-m-d H:i:s');
 #          $post["message"] = $status_update->text;
 #          $post["person"] = $status_update->user->name;
 #          $post["picture"] = $status_update->user->profile_image_url;
 #          $crud_model->insertNewest("lab_twitter", $post, "status_id");
 #        endforeach;
 #      }
 #    endforeach;
 #  }

 #  public function action_update()
 #  {
 #    $this->_writecommits();
 #    $this->_writeTwitterMembers();
 #    $this->action_submit();
 #  }


 #  //Getter methods
 #  private function _commits()
 #  {
 #    //$site_passphrase = Kohana::$config->load('site')->get('passphrase');
 #    //$bit_bucket_users = Kohana::$config->load('site')->get('bitbucket_users');
 #    $crud_model = new Model_Crud();
 #    return $crud_model->getRecentCommits("commits", 6);
 # }

 #  private function _pygenewiki()
 #  {
 #    $crud_model = new Model_Crud();
 #    return $crud_model->getRecent("pygenewiki", 6);
 #  }

 #  private function _lab_twitter()
 #  {
 #    $crud_model = new Model_Crud();
 #    return $crud_model->getRecent("lab_twitter", 6, "timestamp");
 #  }

 #  private function _at_lab_twitter()
 #  {
 #    $crud_model = new Model_Crud();
 #    return $crud_model->getRecent("at_lab_twitter", 6, "id");
 #  }

 #  public function action_poll()
 #  {
 #    $results = array();
 #    $results["bitbucket"] = $this->_commits();
 #    $results["pygenewiki"] = $this->_pygenewiki();
 #    $results["lab_twitter"] = $this->_lab_twitter();
 #    $results["at_lab_twitter"] = $this->_at_lab_twitter();
 #    $this->response->headers('content-type', 'application/json')->body(json_encode($results));
 #  }
