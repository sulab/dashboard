class Log
  include DataMapper::Resource
  property :id,         Serial
  property :application,Integer
  property :version,    String, :length => 12
  property :status,     Integer
  property :header,     String, :length => 255
  property :body,       Text
  property :timestamp,  DateTime
end

class Commit
  include DataMapper::Resource
  property :id,         Serial
  property :author,     String, :length => 255
  property :repo,       String, :length => 255
  property :message,    Text
  property :timestamp,  DateTime
  # property :hash,       String, :length => 40
end

class Mapping
  include DataMapper::Resource
  property :id,          Serial

  property :entrez_id,   Integer
  #property :title_text,  Text
  property :title_url,   String, :length => 255

  property :updated,     DateTime
  property :hits,        Integer
end

DataMapper.finalize
