require 'json'
require 'nokogiri'
require 'rss'

module Get
  
  def self.XML(url)
    body = self.body(url)
    Nokogiri::XML(body) rescue nil
  end
  
  def self.HTML(url)
    body = self.body(url)
    Nokogiri::HTML(body) rescue nil
  end
  
  def self.JSON(url, params={})
    body = self.body(url)
    begin
      return JSON.parse(body, params)
    rescue Exception => e
      LOGGER.error e
      return nil
    end
  end
  
  def self.RSS(url)
    body = self.body(url)
    RSS::Parser.parse(body, false) rescue nil
  end
  
  def self.body(url, tries=3)
    body = nil
    1.upto(3) do |try|
      begin
        body = open(url).read
      rescue Exception => e
        LOGGER.error e
        body = nil
      end
      break if !body.nil?
    end
    return body
  end
  
end
