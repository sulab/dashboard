var DASH = {
  init : function() {
    DASH.drawStreamgraph();
    $.ajax({
      url: 'log/gen_report',
      success: function(d) {

        DASH.drawPieGraph([{v:d.pygenewiki.success}, {v:d.pygenewiki.warning}, {v:d.pygenewiki.error}]);
        DASH.drawErrorLog(d.pygenewiki.log);
      }
    });
  },
  drawStreamgraph: function(data) {
    var margin = {top: 20, right: 30, bottom: 30, left: 40},
        width = 960 - margin.left - margin.right,
        height = 500 - margin.top - margin.bottom;

    var data = [[ 1,  4,  2,  7],
                [21,  2,  5, 10],
                [ 1, 17, 16,  6]];

    // permute the data
    data = data.map(function(d) { return d.map(function(p, i) { return {x:i, y:p, y0:0}; }); });

    var colors = d3.scale.category10();

    var x = d3.scale.linear()
        .range([0, width])
        .domain([0,3]);

    var y = d3.scale.linear()
        .range([height, 0])
        .domain([0,25]);

    var z = d3.scale.category20c();

    var svg = d3.select("#streamgraph").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var stack = d3.layout.stack()
        .offset("zero")

    var layers = stack(data);

    var area = d3.svg.area()
        .interpolate('cardinal')
        .x(function(d, i) { return x(i); })
        .y0(function(d) { return y(d.y0); })
        .y1(function(d) { return y(d.y0 + d.y); });

    svg.selectAll(".layer")
          .data(layers)
          .enter().append("path")
          .attr("class", "layer")
          .attr("d", function(d) { return area(d); })
          .style("fill", function(d, i) { return colors(i); });
  },
  drawErrorLog: function(data) {
    var height = 100,
        width = 500,
        color = ["#5bbae1", "#ffe38c", "#ff4444"];

    var tooltip = d3.select("#chart .tooltip")
        .style("position", "absolute")
        .style("z-index", "10")

    var svg = d3.select("#chart").append("svg:svg").attr('height', height),
        rects = svg.selectAll("rect").data(data);

    rects.enter().append("rect")
        .attr('width', 1)
        .attr('height', 100)
        .attr('x', function(d, i){ return i; })
        .attr('y', '0px')
        .attr("fill", function(d, i) {
          return color[d["status"]]
        })
        .on("mouseover", function(obj) {
          return tooltip
                  .style("top", (event.pageY-10)+"px")
                  .style("left",(event.pageX+10)+"px")
                  .style("color", function() {
                    if( obj.status == 0 ) {
                      return "#000";
                    } else {
                      return "red";
                    }
                  })
                  .text( obj.status + " : " +obj.header );
        });

  },
  drawPieGraph : function(data) {
    var w = 400,
        h = 400,
        r = Math.min(w, h)/4,
        labelr = r + 10, // radius for label anchor
        color = ["#5bbae1", "#ffe38c", "#ff4444"],
        donut = d3.layout.pie(),
        arc = d3.svg.arc().innerRadius(0).outerRadius(r);

    var vis = d3.select("#piegraph")
        .append("svg:svg")
        .data([data])
        .attr("width", w + 20)
        .attr("height", h + 20);

    var arcs = vis.selectAll("g.arc")
          .data(donut.value(function(d) { return d.v }))
          .enter().append("svg:g")
          .attr("class", "arc")
          .attr("transform", "translate(" + (r+20) + "," + (r+20) + ")");

    arcs.append("svg:path")
          .attr("fill", function(d, i) { return color[i]; })
          .attr("d", arc);

    arcs.append("svg:text")
      .attr("transform", function(d) {
      var c = arc.centroid(d),
        x = c[0],
        y = c[1],
        // pythagorean theorem for hypotenuse
        h = Math.sqrt(x*x + y*y);
        return "translate(" + (x/h * labelr) +  ',' + (y/h * labelr) +  ")"; 
    })
    .attr("dy", ".35em")
    .attr("text-anchor", function(d) {
      // are we past the center?
      return (d.endAngle + d.startAngle)/2 > Math.PI ? "end" : "start";
    })
    .attr("font-family", "Helvetica")
    .text(function(d){ if(d.value > 0) { return d.value; } });
  }
}

$(document).ready( DASH.init() );
