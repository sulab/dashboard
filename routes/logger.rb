# Remote code status report submissions
post '/log/submit' do
  Log.create(  :application => params['application'],
               :version => params['version'],
               :status => params['status'],
               :header => params['page_title'],
               :body => params['message'],
               :timestamp => DateTime.now )
end

post '/log/relay' do
  status = params['status'] # 0 -- hipchat (logging room) only, 1 -- email + hipchat
  msg = params['msg'] # Content of the message, can be multilined (\n)
  to = params['recipients'] # all if a sting of emails aren't defined "foo@scripps.edu, John Doe <bar@scripps.edu>"
  
  return if [status, msg].include? nil
  color = (status=="0" ? "green" : "red")
  hipchat_sender(msg, color)
  email_sender(msg, "Su Lab Notifier", to) if status=="1"
end

get '/log/gen_report' do
  content_type :json
  report_generation.to_json
end

get '/log/weekly_report' do
  report = weekly_report[:pygenewiki]

  rate = (( report[:warning] + report[:error] ) / report[:total].to_f  * 100 ).round(4) rescue 0
  updated_per = ( report[:success] / report[:total].to_f * 100).round(4) rescue 0

  errors = "Top warning issues:\n"
  report[:error_log].sort_by {|k,v| v}.reverse.each do |e|
    errors << "    #{e[0]} (#{ e[1] })\n"
  end

  msg = "A total of #{ report[:total] } pages were checked while #{ report[:success] } (#{updated_per}%) pages were successfully edited.\n\n There was a total of #{ report[:warning] + report[:error] } (#{ rate }%) issues where #{ report[:warning] } give us some notification warning and #{ report[:error] } threw a complete error.\n\n #{errors}"

  color = 'red'
  if rate < 1.0 then color = 'green' end
  hipchat_sender(msg, color)
  email_sender(msg, "Weekly (##{Date.today.cweek}) Su Lab Report", $yaml["etc"]["email_recipients_select"])
end
