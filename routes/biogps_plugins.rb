get '/biogps-plugins/wp/?:id?' do |id|
  id = id || params[:id]
  if id.nil?
    slim:'biogps-plugins/wp'
  elsif( id =~ /^[0-9]*$/ )
    # confirmed that we were passed a valid formatted id
    map = Mapping.first(:entrez_id => id) rescue nil
    target = map[:title_url] if !map.nil?
      map.update(:hits => map[:hits]+1) if !map.nil?
    redirect URI.encode("http://en.wikipedia.org/wiki/#{target}") if !map.nil?
    redirect URI.encode("http://plugins.biogps.org/cgi-bin/gwgenerator.cgi?id=#{id}")
  else
    #if it didn't match the format of a entrez_id
    redirect URI.encode("http://en.wikipedia.org/wiki/Special:Search?ns0=1&search=%22Symbol+%3D+#{id}%22&fulltext=Search")
  end
end

get '/biogps-plugins/freebase/?:id?' do |id|
  id = id || params[:id]
  if id.nil?
    slim:'biogps-plugins/freebase'
  end
  #elsif( id =~ /^[0-9]*$/ )
  #i no idea what that perl json stuff is doing
end

get '/biogps-plugins/primerbank/?:id?' do |id|
  @id = id || params[:id]
  slim:'biogps-plugins/primerbank'
end
